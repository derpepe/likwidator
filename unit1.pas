unit Unit1;

{$mode objfpc}{$H+}

{
This file is part of Likwidator.

Likwidator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Keulkulator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Likwidator.  If not, see <http://www.gnu.org/licenses/>.
}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Spin,
  Buttons, Menus, ComCtrls, Grids, LCLType, UData;

type

  { TForm1 }

  TForm1 = class(TForm)
    BtBerechnen: TButton;
    EdAromaBezeichnung: TEdit;
    EdRezeptBase: TEdit;
    EdRezeptPG: TEdit;
    EdRezeptVG: TEdit;
    EdRezeptWasser: TEdit;
    FlSpEdLiqZutat: TFloatSpinEdit;
    FlSpEdLiquid: TFloatSpinEdit;
    GBxAusgang: TGroupBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label2: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label3: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label4: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    LiBxAromen: TListBox;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    SpEBaseNikotin: TSpinEdit;
    SpELiquidNikotin: TSpinEdit;
    SpEBasePG: TSpinEdit;
    SpEAromaPG: TSpinEdit;
    SpEAromaVG: TSpinEdit;
    SpEAromaWasser: TSpinEdit;
    SpELiquidPG: TSpinEdit;
    SpELiquidVG: TSpinEdit;
    SpELiquidWasser: TSpinEdit;
    SpeBtAromaAdd: TSpeedButton;
    SpeBtAromaDel: TSpeedButton;
    SpeBtAromaEdit: TSpeedButton;
    SpEPGPG: TSpinEdit;
    SpEBaseVG: TSpinEdit;
    SpEVGVG: TSpinEdit;
    SpEBaseWasser: TSpinEdit;
    SpEVGWasser1: TSpinEdit;
    SpEPGWasser: TSpinEdit;
    StatusBar1: TStatusBar;
    StrGrLiqZutaten: TStringGrid;
    StrGrRezeptZutaten1: TStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure SpEAromaPGChange(Sender: TObject);
    procedure SpEAromaVGChange(Sender: TObject);
    procedure SpEAromaWasserChange(Sender: TObject);
    procedure SpEBaseNikotinChange(Sender: TObject);
    procedure SpEBasePGChange(Sender: TObject);
    procedure SpEBaseVGChange(Sender: TObject);
    procedure SpEBaseWasserChange(Sender: TObject);
    procedure SpeBtAromaAddClick(Sender: TObject);
    procedure SpELiquidPGChange(Sender: TObject);
    procedure SpELiquidVGChange(Sender: TObject);
    procedure SpELiquidWasserChange(Sender: TObject);
    procedure SpEPGPGChange(Sender: TObject);
    procedure SpEPGWasserChange(Sender: TObject);
    procedure SpEVGVGChange(Sender: TObject);
    procedure SpEVGWasser1Change(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }


procedure TForm1.MenuItem2Click(Sender: TObject);
begin
end;

procedure TForm1.SpEAromaPGChange(Sender: TObject);
var ArSumme, ArPG, ArVG, ArWa : integer;
begin
  ArPG := SpEAromaPG.Value;
  ArVG := SpEAromaVG.Value;
  ArWa := SpEAromaWasser.Value;
  ArSumme := ArPG + ArVG + ArWa;
  if ArSumme <> 100 then
    ArVG := 100 - ArPG - ArWa;
  SpEAromaPG.Value := ArPG;
  SpEAromaVG.Value := ArVG;
  SpEAromaWasser.Value := ArWa;
end;

procedure TForm1.SpEAromaVGChange(Sender: TObject);
var ArSumme, ArPG, ArVG, ArWa : integer;
begin
  ArPG := SpEAromaPG.Value;
  ArVG := SpEAromaVG.Value;
  ArWa := SpEAromaWasser.Value;
  ArSumme := ArPG + ArVG + ArWa;
  if ArSumme <> 100 then
    ArPG := 100 - ArVG - ArWa;
  SpEAromaPG.Value := ArPG;
  SpEAromaVG.Value := ArVG;
  SpEAromaWasser.Value := ArWa;
end;

procedure TForm1.SpEAromaWasserChange(Sender: TObject);
var ArSumme, ArPG, ArVG, ArWa : integer;
begin
  ArPG := SpEAromaPG.Value;
  ArVG := SpEAromaVG.Value;
  ArWa := SpEAromaWasser.Value;
  ArSumme := ArPG + ArVG + ArWa;
  if ArSumme <> 100 then
    ArPG := 100 - ArVG - ArWa;
  SpEAromaPG.Value := ArPG;
  SpEAromaVG.Value := ArVG;
  SpEAromaWasser.Value := ArWa;
end;

procedure TForm1.SpEBaseNikotinChange(Sender: TObject);
begin
  SpELiquidNikotin.MaxValue:=SpEBaseNikotin.Value;
end;

procedure TForm1.SpEBasePGChange(Sender: TObject);
var NBSumme, NBPG, NBVG, NBWa : integer;
begin
  NBPG := SpeBasePG.Value;
  NBVG := SpEBaseVG.Value;
  NBWa := SpEBaseWasser.Value;
  NBSumme := NBPG + NBVG + NBWa;
  if NBSumme <> 100 then
     NBVG := 100 - NBPG - NBWa;
  SpeBasePG.Value := NBPG;
  SpEBaseVG.Value := NBVG;
  SpEBaseWasser.Value := NBWa;
end;

procedure TForm1.SpEBaseVGChange(Sender: TObject);
var NBSumme, NBPG, NBVG, NBWa : integer;
begin
  NBPG := SpeBasePG.Value;
  NBVG := SpEBaseVG.Value;
  NBWa := SpEBaseWasser.Value;
  NBSumme := NBPG + NBVG + NBWa;
  if NBSumme <> 100 then
    NBPG := 100 - NBVG - NBWa;
  SpeBasePG.Value := NBPG;
  SpEBaseVG.Value := NBVG;
  SpEBaseWasser.Value := NBWa;
end;

procedure TForm1.SpEBaseWasserChange(Sender: TObject);
var NBSumme, NBPG, NBVG, NBWa : integer;
begin
  NBPG := SpeBasePG.Value;
  NBVG := SpEBaseVG.Value;
  NBWa := SpEBaseWasser.Value;
  NBSumme := NBPG + NBVG + NBWa;
  if NBSumme <> 100 then
    NBPG := 100 - NBVG - NBWa;
  SpeBasePG.Value := NBPG;
  SpEBaseVG.Value := NBVG;
  SpEBaseWasser.Value := NBWa;
end;

procedure TForm1.SpeBtAromaAddClick(Sender: TObject);
var dummy : integer;
begin
  if AnzAromen <= 6 then
     if (EdAromaBezeichnung.Text <> '') and (AnzAromen <= 6) then
       begin
         AnzAromen := AnzAromen + 1;
         Aromen[AnzAromen].ABezeichnung := EdAromaBezeichnung.Text;
         Aromen[AnzAromen].APG := SpEAromaPG.Value;
         Aromen[AnzAromen].AVG := SpEAromaVG.Value;
         Aromen[AnzAromen].AWa := SpEAromaWasser.Value;
         LiBxAromen.Items.Add(EdAromaBezeichnung.Text);
         StrGrLiqZutaten.RowCount := StrGrLiqZutaten.RowCount + 1;
         StrGrLiqZutaten.Cells[1,AnzAromen]:=Aromen[AnzAromen].ABezeichnung;
         StrGrLiqZutaten.Cells[2,AnzAromen]:='0.00';
         EdAromaBezeichnung.Text := '';
         SpEAromaPG.Value := 100;
         SpEAromaVG.Value := 0;
         SpEAromaWasser.Value := 0;
       end
     else
       dummy := Application.MessageBox('Bezeichnung darf nicht leer sein', 'Bezeichnung fehlt', MB_OK)
  else
    dummy := Application.MessageBox('Maximal 7 Zutaten möglich', 'Liste voll', MB_OK)
end;

procedure TForm1.SpELiquidPGChange(Sender: TObject);
var LiSumme, LiPG, LiVG, LiWa : integer;
begin
  LiPG := SpELiquidPG.Value;
  LiVG := SpELiquidVG.Value;
  LiWa := SpELiquidWasser.Value;
  LiSumme := LiPG + LiVG + LiWa;
  if LiSumme <> 100 then
    LiVG := 100 - LiPG - LiWa;
  SpELiquidPG.Value := LiPG;
  SpELiquidVG.Value := LiVG;
  SpELiquidWasser.Value := LiWa;
end;

procedure TForm1.SpELiquidVGChange(Sender: TObject);
var LiSumme, LiPG, LiVG, LiWa : integer;
begin
  LiPG := SpELiquidPG.Value;
  LiVG := SpELiquidVG.Value;
  LiWa := SpELiquidWasser.Value;
  LiSumme := LiPG + LiVG + LiWa;
  if LiSumme <> 100 then
    LiPG := 100 - LiVG - LiWa;
  SpELiquidPG.Value := LiPG;
  SpELiquidVG.Value := LiVG;
  SpELiquidWasser.Value := LiWa;
end;

procedure TForm1.SpELiquidWasserChange(Sender: TObject);
  var LiSumme, LiPG, LiVG, LiWa : integer;
begin
  LiPG := SpELiquidPG.Value;
  LiVG := SpELiquidVG.Value;
  LiWa := SpELiquidWasser.Value;
  LiSumme := LiPG + LiVG + LiWa;
  if LiSumme <> 100 then
    LiPG := 100 - LiVG - LiWa;
  SpELiquidPG.Value := LiPG;
  SpELiquidVG.Value := LiVG;
  SpELiquidWasser.Value := LiWa;
end;

procedure TForm1.SpEPGPGChange(Sender: TObject);
var PBSumme, PBPG, PBWa : integer;
begin
  PBPG := SpEPGPG.Value;
  PBWa := SpEPGWasser.Value;
  PBSumme := PBPG + PBWa;
  if PBSumme <> 100 then
    PBWa := 100 - PBPG;
  SpEPGPG.Value := PBPG;
  SpEPGWasser.Value := PBWa;
end;

procedure TForm1.SpEPGWasserChange(Sender: TObject);
var PBSumme, PBPG, PBWa : integer;
begin
  PBPG := SpEPGPG.Value;
  PBWa := SpEPGWasser.Value;
  PBSumme := PBPG + PBWa;
  if PBSumme <> 100 then
    PBPG := 100 - PBWa;
  SpEPGPG.Value := PBPG;
  SpEPGWasser.Value := PBWa;
end;

procedure TForm1.SpEVGVGChange(Sender: TObject);
var VBSumme, VBVG, VBWa : integer;
begin
  VBVG := SpEVGVG.Value;
  VBWa := SpEVGWasser1.Value;
  VBSumme := VBVG + VBWa;
  if VBSumme <> 100 then
    VBWa := 100 - VBVG;
    SpEVGVG.Value := VBVG;
    SpEVGWasser1.Value := VBWa;
end;

procedure TForm1.SpEVGWasser1Change(Sender: TObject);
var VBSumme, VBVG, VBWa : integer;
begin
  VBVG := SpEVGVG.Value;
  VBWa := SpEVGWasser1.Value;
  VBSumme := VBVG + VBWa;
  if VBSumme <> 100 then
    VBVG := 100 - VBWa;
    SpEVGVG.Value := VBVG;
    SpEVGWasser1.Value := VBWa;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  InitData;
end;

end.

