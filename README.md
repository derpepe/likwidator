# Likwidator

Ein Programm für die Berechnung der Zutatenmengen für das Selbstmischen von Liquids für mobile Liquidzertäuber („E-Zigaretten“).

![Screenshot](pic/Screenshot_20210628_234822.png)



## Dokumentation

Die Dokumentation befindet sich im Unterverzeichnis docs. Die Hilfe entweder direkt aus dem Programm aufrufen oder die Datei help.html bzw. help.md öffnen.



## Installation





## Erstellen



## Lizenz

Likwidator ist lizensiert unter der [GNU General Public License Version 3](https://www.gnu.org/licenses/gpl.txt).

[GNU GPL 3](gpl.txt)



Copyright 2021 PepeCyB