unit UData;

{$mode objfpc}{$H+}

{
This file is part of Likwidator.

Likwidator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Keulkulator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Likwidator.  If not, see <http://www.gnu.org/licenses/>.
}

interface

uses
  Classes, SysUtils;

  procedure InitData;

type
  TNBase = record
             BNikotin : integer;
             BPG : integer;
             BVG : integer;
             BWa : integer;
           end;

  TPGBase = record
              BPG : integer;
              BWa : integer;
            end;

  TVGBase = record
              BVG : integer;
              BWa : integer;
            end;

  TAroma = record
             ABezeichnung : string;
             APG : integer;
             AVG : integer;
             AWa : integer;
           end;

  TLiquid = record
              LMenge : real;
              LNikotin : integer;
              LPG : integer;
              LVG : integer;
              LWA : integer;
              LZutaten : array[1..7] of real;
            end;

  TRezept = record
              RNikotin : real;
              RPG : real;
              RVG : real;
              RWa : real;
              RZutaten : array[1..7] of real;
            end;

var
  NikotinBase : TNBase;
  PropGlyk : TPGBase;
  Glycerin : TVGBase;
  Aromen : array[1..7] of TAroma;
  AnzAromen : integer;
  Liquid : TLiquid;
  Rezept : TRezept;

implementation

function MyStrToFloat(s: String): Double;  // eigene Umwandlungsfunktion, um Systemunterschiede
var                                        // bezüglich des Dezimaltrenners aufzufangen
  fs: TFormatSettings;
begin
  fs := DefaultFormatSettings;
  if not TryStrToFloat(s, Result, fs) then
  begin
    if fs.DecimalSeparator = '.' then
      fs.DecimalSeparator := ','
    else
      fs.DecimalSeparator := '.';
    Result := StrToFloat(s, fs);
  end;
end;

procedure InitData;                // Werte initialisieren

var i : integer;

begin
  NikotinBase.BNikotin:=20;
  NikotinBase.BPG:=100;
  NikotinBase.BVG:=0;
  NikotinBase.BWa:=0;

  PropGlyk.BPG:=100;
  PropGlyk.BWa:=0;

  Glycerin.BVG:=100;
  Glycerin.BWa:=0;

  for i := 1 to 7 do
  begin
    Aromen[i].ABezeichnung:='';
    Aromen[i].APG:=0;
    Aromen[i].AVG:=0;
    Aromen[i].AWa:=0;
  end;
  AnzAromen := 0;

  Liquid.LMenge:=100.0;
  Liquid.LNikotin:=0;
  Liquid.LPG:=55;
  Liquid.LVG:=35;
  Liquid.LWA:=10;
  for i:=1 to 7 do
    Liquid.LZutaten[i]:=0.0;

  Rezept.RNikotin:=0.0;
  Rezept.RPG:=55.0;
  Rezept.RVG:=35.0;
  for i:=1 to 7 do
    Rezept.RZutaten[i]:=0.0;
end;

end.

